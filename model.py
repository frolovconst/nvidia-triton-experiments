import torch
import numpy as np
from transformers import LlamaTokenizer, LlamaForCausalLM
import triton_python_backend_utils as pb_utils


class TritonPythonModel:
    def __init__(self):
        self.model = None
        self.tokenizer = None

    def initialize(self, args):
        model_path = 'openlm-research/open_llama_3b_v2'
        self.tokenizer = LlamaTokenizer.from_pretrained(model_path)
        self.model = LlamaForCausalLM.from_pretrained(
            model_path, torch_dtype=torch.float16, device_map='auto',
        )

    def execute(self, requests):
        responses = []
        for request in requests:
            prompt = pb_utils.get_input_tensor_by_name(request, 'prompt').as_numpy()
            prompt = prompt[0].decode("utf-8")
            input_ids = self.tokenizer(prompt, return_tensors="pt").input_ids.to("cuda")
            generation_output = self.model.generate(
                input_ids=input_ids, max_new_tokens=32
            )
            text = self.tokenizer.decode(generation_output[0])
            text = np.array([text.encode('utf-8')], dtype = np.object_)
            out_tensor = pb_utils.Tensor("llm_response", text)
            responses.append(pb_utils.InferenceResponse([out_tensor]))
        return responses

